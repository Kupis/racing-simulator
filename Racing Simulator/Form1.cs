﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Racing_Simulator
{
    public partial class mainForm : Form
    {
        Greyhound[] greyhoundArray;
        Person[] personArray;

        public mainForm()
        {
            InitializeComponent();

           greyhoundArray = new Greyhound[4] 
           {
                new Greyhound() { myPictureBox = greyhoundPictureBox1, startingPosition = greyhoundPictureBox1.Left, racetrackLength = racetrackPictureBox.Width - greyhoundPictureBox1.Width },
                new Greyhound() { myPictureBox = greyhoundPictureBox2, startingPosition = greyhoundPictureBox2.Left, racetrackLength = racetrackPictureBox.Width - greyhoundPictureBox2.Width },
                new Greyhound() { myPictureBox = greyhoundPictureBox3, startingPosition = greyhoundPictureBox3.Left, racetrackLength = racetrackPictureBox.Width - greyhoundPictureBox3.Width },
                new Greyhound() { myPictureBox = greyhoundPictureBox4, startingPosition = greyhoundPictureBox4.Left, racetrackLength = racetrackPictureBox.Width - greyhoundPictureBox4.Width }
           };

            personArray = new Person[3]
            {
                new Person() { name = "Janusz", cash = 50, myRadioButon = januszRadioButton, myTextBox = januszTextBox },
                new Person() { name = "Mietek", cash = 50, myRadioButon = mietekRadioButton, myTextBox = mietekTextBox },
                new Person() { name = "Seba", cash = 50, myRadioButon = sebaRadioButton, myTextBox = sebaTextBox }
            };

            updateRadioButton();
        }

        private void updateRadioButton()
        {
            foreach (var person in personArray)
            {
                person.updateRadioButton();
            }
        }

        private void betButton_Click(object sender, EventArgs e)
        {
            foreach (var person in personArray)
            {
                if (person.myRadioButon.Checked)
                {
                    person.PlaceBet((int)betMoneyNumeric.Value, (int)(betGreyhoundNumeric.Value - 1));
                    break;
                }
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            bookmakerGroup.Enabled = false;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                if(greyhoundArray[i].Run())
                {
                    timer1.Stop();
                    MessageBox.Show("Greyhound number " + (i+1).ToString() + " is winner!");
                    giveOrTakeMoney(i);
                    resetDogs();
                    bookmakerGroup.Enabled = true;
                    break;
                }
            }
        }

        private void giveOrTakeMoney(int winner)
        {
            foreach (var person in personArray)
            {
                if (person.isBetting)
                {
                    person.Collect(winner);
                }
            }
        }

        private void resetDogs()
        {
            foreach(var dog in greyhoundArray)
            {
                dog.TakeStartingPosition();
            }

            clearTextBox();
        }

        private void clearTextBox()
        {
            januszTextBox.Text = "";
            mietekTextBox.Text = "";
            sebaTextBox.Text = "";
        }

        private void januszRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            selectedPersonLabel.Text = "Janusz";
        }

        private void mietekRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            selectedPersonLabel.Text = "Mietek";
        }

        private void sebaRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            selectedPersonLabel.Text = "Seba";
        }
    }
}
