﻿namespace Racing_Simulator
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.greyhoundPictureBox4 = new System.Windows.Forms.PictureBox();
            this.greyhoundPictureBox3 = new System.Windows.Forms.PictureBox();
            this.greyhoundPictureBox2 = new System.Windows.Forms.PictureBox();
            this.greyhoundPictureBox1 = new System.Windows.Forms.PictureBox();
            this.racetrackPictureBox = new System.Windows.Forms.PictureBox();
            this.bookmakerGroup = new System.Windows.Forms.GroupBox();
            this.startButton = new System.Windows.Forms.Button();
            this.betDescriptionLabel = new System.Windows.Forms.Label();
            this.betGreyhoundNumeric = new System.Windows.Forms.NumericUpDown();
            this.betMoneyNumeric = new System.Windows.Forms.NumericUpDown();
            this.betButton = new System.Windows.Forms.Button();
            this.selectedPersonLabel = new System.Windows.Forms.Label();
            this.sebaTextBox = new System.Windows.Forms.TextBox();
            this.mietekTextBox = new System.Windows.Forms.TextBox();
            this.januszTextBox = new System.Windows.Forms.TextBox();
            this.sebaRadioButton = new System.Windows.Forms.RadioButton();
            this.mietekRadioButton = new System.Windows.Forms.RadioButton();
            this.januszRadioButton = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.racetrackPictureBox)).BeginInit();
            this.bookmakerGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.betGreyhoundNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.betMoneyNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // greyhoundPictureBox4
            // 
            this.greyhoundPictureBox4.Image = global::Racing_Simulator.Properties.Resources.dog;
            this.greyhoundPictureBox4.Location = new System.Drawing.Point(12, 180);
            this.greyhoundPictureBox4.Name = "greyhoundPictureBox4";
            this.greyhoundPictureBox4.Size = new System.Drawing.Size(75, 20);
            this.greyhoundPictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.greyhoundPictureBox4.TabIndex = 4;
            this.greyhoundPictureBox4.TabStop = false;
            // 
            // greyhoundPictureBox3
            // 
            this.greyhoundPictureBox3.Image = global::Racing_Simulator.Properties.Resources.dog;
            this.greyhoundPictureBox3.Location = new System.Drawing.Point(12, 125);
            this.greyhoundPictureBox3.Name = "greyhoundPictureBox3";
            this.greyhoundPictureBox3.Size = new System.Drawing.Size(75, 20);
            this.greyhoundPictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.greyhoundPictureBox3.TabIndex = 3;
            this.greyhoundPictureBox3.TabStop = false;
            // 
            // greyhoundPictureBox2
            // 
            this.greyhoundPictureBox2.Image = global::Racing_Simulator.Properties.Resources.dog;
            this.greyhoundPictureBox2.Location = new System.Drawing.Point(12, 72);
            this.greyhoundPictureBox2.Name = "greyhoundPictureBox2";
            this.greyhoundPictureBox2.Size = new System.Drawing.Size(75, 20);
            this.greyhoundPictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.greyhoundPictureBox2.TabIndex = 2;
            this.greyhoundPictureBox2.TabStop = false;
            // 
            // greyhoundPictureBox1
            // 
            this.greyhoundPictureBox1.Image = global::Racing_Simulator.Properties.Resources.dog;
            this.greyhoundPictureBox1.Location = new System.Drawing.Point(12, 22);
            this.greyhoundPictureBox1.Name = "greyhoundPictureBox1";
            this.greyhoundPictureBox1.Size = new System.Drawing.Size(75, 20);
            this.greyhoundPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.greyhoundPictureBox1.TabIndex = 1;
            this.greyhoundPictureBox1.TabStop = false;
            // 
            // racetrackPictureBox
            // 
            this.racetrackPictureBox.Image = global::Racing_Simulator.Properties.Resources.racetrack;
            this.racetrackPictureBox.Location = new System.Drawing.Point(12, 12);
            this.racetrackPictureBox.Name = "racetrackPictureBox";
            this.racetrackPictureBox.Size = new System.Drawing.Size(600, 200);
            this.racetrackPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.racetrackPictureBox.TabIndex = 0;
            this.racetrackPictureBox.TabStop = false;
            // 
            // bookmakerGroup
            // 
            this.bookmakerGroup.Controls.Add(this.startButton);
            this.bookmakerGroup.Controls.Add(this.betDescriptionLabel);
            this.bookmakerGroup.Controls.Add(this.betGreyhoundNumeric);
            this.bookmakerGroup.Controls.Add(this.betMoneyNumeric);
            this.bookmakerGroup.Controls.Add(this.betButton);
            this.bookmakerGroup.Controls.Add(this.selectedPersonLabel);
            this.bookmakerGroup.Controls.Add(this.sebaTextBox);
            this.bookmakerGroup.Controls.Add(this.mietekTextBox);
            this.bookmakerGroup.Controls.Add(this.januszTextBox);
            this.bookmakerGroup.Controls.Add(this.sebaRadioButton);
            this.bookmakerGroup.Controls.Add(this.mietekRadioButton);
            this.bookmakerGroup.Controls.Add(this.januszRadioButton);
            this.bookmakerGroup.Controls.Add(this.label2);
            this.bookmakerGroup.Controls.Add(this.label1);
            this.bookmakerGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bookmakerGroup.Location = new System.Drawing.Point(12, 218);
            this.bookmakerGroup.Name = "bookmakerGroup";
            this.bookmakerGroup.Size = new System.Drawing.Size(600, 172);
            this.bookmakerGroup.TabIndex = 5;
            this.bookmakerGroup.TabStop = false;
            this.bookmakerGroup.Text = "Bookmaker\'s house";
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(462, 47);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(132, 115);
            this.startButton.TabIndex = 13;
            this.startButton.Text = "Start!";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // betDescriptionLabel
            // 
            this.betDescriptionLabel.AutoSize = true;
            this.betDescriptionLabel.Location = new System.Drawing.Point(173, 141);
            this.betDescriptionLabel.Name = "betDescriptionLabel";
            this.betDescriptionLabel.Size = new System.Drawing.Size(148, 16);
            this.betDescriptionLabel.TabIndex = 12;
            this.betDescriptionLabel.Text = "$ on greyhound number";
            // 
            // betGreyhoundNumeric
            // 
            this.betGreyhoundNumeric.Location = new System.Drawing.Point(327, 139);
            this.betGreyhoundNumeric.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.betGreyhoundNumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.betGreyhoundNumeric.Name = "betGreyhoundNumeric";
            this.betGreyhoundNumeric.Size = new System.Drawing.Size(40, 22);
            this.betGreyhoundNumeric.TabIndex = 11;
            this.betGreyhoundNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.betGreyhoundNumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // betMoneyNumeric
            // 
            this.betMoneyNumeric.Location = new System.Drawing.Point(124, 139);
            this.betMoneyNumeric.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.betMoneyNumeric.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.betMoneyNumeric.Name = "betMoneyNumeric";
            this.betMoneyNumeric.Size = new System.Drawing.Size(43, 22);
            this.betMoneyNumeric.TabIndex = 10;
            this.betMoneyNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.betMoneyNumeric.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // betButton
            // 
            this.betButton.AutoSize = true;
            this.betButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.betButton.Location = new System.Drawing.Point(81, 136);
            this.betButton.Name = "betButton";
            this.betButton.Size = new System.Drawing.Size(37, 26);
            this.betButton.TabIndex = 9;
            this.betButton.Text = "bet";
            this.betButton.UseVisualStyleBackColor = true;
            this.betButton.Click += new System.EventHandler(this.betButton_Click);
            // 
            // selectedPersonLabel
            // 
            this.selectedPersonLabel.AutoSize = true;
            this.selectedPersonLabel.Location = new System.Drawing.Point(30, 141);
            this.selectedPersonLabel.Name = "selectedPersonLabel";
            this.selectedPersonLabel.Size = new System.Drawing.Size(45, 16);
            this.selectedPersonLabel.TabIndex = 8;
            this.selectedPersonLabel.Text = "Janek";
            // 
            // sebaTextBox
            // 
            this.sebaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sebaTextBox.Location = new System.Drawing.Point(148, 102);
            this.sebaTextBox.Name = "sebaTextBox";
            this.sebaTextBox.ReadOnly = true;
            this.sebaTextBox.Size = new System.Drawing.Size(308, 22);
            this.sebaTextBox.TabIndex = 7;
            // 
            // mietekTextBox
            // 
            this.mietekTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mietekTextBox.Location = new System.Drawing.Point(148, 74);
            this.mietekTextBox.Name = "mietekTextBox";
            this.mietekTextBox.ReadOnly = true;
            this.mietekTextBox.Size = new System.Drawing.Size(308, 22);
            this.mietekTextBox.TabIndex = 6;
            // 
            // januszTextBox
            // 
            this.januszTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.januszTextBox.Location = new System.Drawing.Point(148, 46);
            this.januszTextBox.Name = "januszTextBox";
            this.januszTextBox.ReadOnly = true;
            this.januszTextBox.Size = new System.Drawing.Size(308, 22);
            this.januszTextBox.TabIndex = 5;
            // 
            // sebaRadioButton
            // 
            this.sebaRadioButton.AutoSize = true;
            this.sebaRadioButton.Location = new System.Drawing.Point(9, 102);
            this.sebaRadioButton.Name = "sebaRadioButton";
            this.sebaRadioButton.Size = new System.Drawing.Size(109, 20);
            this.sebaRadioButton.TabIndex = 4;
            this.sebaRadioButton.TabStop = true;
            this.sebaRadioButton.Text = "Seba have $0";
            this.sebaRadioButton.UseVisualStyleBackColor = true;
            this.sebaRadioButton.CheckedChanged += new System.EventHandler(this.sebaRadioButton_CheckedChanged);
            // 
            // mietekRadioButton
            // 
            this.mietekRadioButton.AutoSize = true;
            this.mietekRadioButton.Location = new System.Drawing.Point(9, 74);
            this.mietekRadioButton.Name = "mietekRadioButton";
            this.mietekRadioButton.Size = new System.Drawing.Size(113, 20);
            this.mietekRadioButton.TabIndex = 3;
            this.mietekRadioButton.TabStop = true;
            this.mietekRadioButton.Text = "Mietek have$0";
            this.mietekRadioButton.UseVisualStyleBackColor = true;
            this.mietekRadioButton.CheckedChanged += new System.EventHandler(this.mietekRadioButton_CheckedChanged);
            // 
            // januszRadioButton
            // 
            this.januszRadioButton.AutoSize = true;
            this.januszRadioButton.Location = new System.Drawing.Point(9, 46);
            this.januszRadioButton.Name = "januszRadioButton";
            this.januszRadioButton.Size = new System.Drawing.Size(118, 20);
            this.januszRadioButton.TabIndex = 2;
            this.januszRadioButton.TabStop = true;
            this.januszRadioButton.Text = "Janusz have $0";
            this.januszRadioButton.UseVisualStyleBackColor = true;
            this.januszRadioButton.CheckedChanged += new System.EventHandler(this.januszRadioButton_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(145, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bets";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Minimal bet: $5";
            // 
            // timer1
            // 
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 402);
            this.Controls.Add(this.bookmakerGroup);
            this.Controls.Add(this.greyhoundPictureBox4);
            this.Controls.Add(this.greyhoundPictureBox3);
            this.Controls.Add(this.greyhoundPictureBox2);
            this.Controls.Add(this.greyhoundPictureBox1);
            this.Controls.Add(this.racetrackPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.Text = "Racing Simulator";
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.greyhoundPictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.racetrackPictureBox)).EndInit();
            this.bookmakerGroup.ResumeLayout(false);
            this.bookmakerGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.betGreyhoundNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.betMoneyNumeric)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox racetrackPictureBox;
        private System.Windows.Forms.PictureBox greyhoundPictureBox1;
        private System.Windows.Forms.PictureBox greyhoundPictureBox2;
        private System.Windows.Forms.PictureBox greyhoundPictureBox3;
        private System.Windows.Forms.PictureBox greyhoundPictureBox4;
        private System.Windows.Forms.GroupBox bookmakerGroup;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label betDescriptionLabel;
        private System.Windows.Forms.NumericUpDown betGreyhoundNumeric;
        private System.Windows.Forms.NumericUpDown betMoneyNumeric;
        private System.Windows.Forms.Button betButton;
        private System.Windows.Forms.Label selectedPersonLabel;
        private System.Windows.Forms.TextBox sebaTextBox;
        private System.Windows.Forms.TextBox mietekTextBox;
        private System.Windows.Forms.TextBox januszTextBox;
        private System.Windows.Forms.RadioButton sebaRadioButton;
        private System.Windows.Forms.RadioButton mietekRadioButton;
        private System.Windows.Forms.RadioButton januszRadioButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
    }
}

