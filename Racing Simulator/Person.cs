﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Racing_Simulator
{
    public class Person
    {
        public string name;
        public Bet myBet;
        public int cash;
        public bool isBetting = false;

        public RadioButton myRadioButon;
        public TextBox myTextBox;

        public void UpdateLabels()
        {
            myTextBox.Text = myBet.getDescription();
            updateRadioButton();
        }

        public void updateRadioButton()
        {
            myRadioButon.Text = name + " have $" + cash;
        }

        public void ClearBet()
        {
            myBet = null;
            isBetting = false;
        }

        public bool PlaceBet(int amount, int dogToWin)
        {
            if (cash >= amount)
            {
                isBetting = true;
                myBet = new Bet()
                {
                    dog = dogToWin,
                    amount = amount,
                    bettor = this
                };

                UpdateLabels();
                return true;
            }
            else
            {
                MessageBox.Show("You don't have enought cash!");
                return false;
            }
        }

        public void Collect(int winner)
        {
            cash += myBet.payOut(winner);
            ClearBet();

            updateRadioButton();
        }
    }
}
