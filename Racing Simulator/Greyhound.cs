﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Racing_Simulator
{
    public class Greyhound
    {
        public int startingPosition;
        public int racetrackLength;
        public PictureBox myPictureBox = null;
        public int location = 0;
        public static Random myRandom = new Random();

        public bool Run()
        {
            location += myRandom.Next(1, 4);
            myPictureBox.Location = new Point(location, myPictureBox.Location.Y);

            if (location >= racetrackLength)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void TakeStartingPosition()
        {
            location = startingPosition;
            myPictureBox.Location = new Point(location, myPictureBox.Location.Y);
        }
    }
}
