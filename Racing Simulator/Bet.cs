﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Racing_Simulator
{
    public class Bet
    {
        public int amount;
        public int dog;
        public Person bettor;

        public string getDescription()
        {
            if (amount >= 5 && amount <= 15)
            {
                return bettor.name + " placed " + amount + " bet on greyhound number " + (dog + 1);
            }
            else
            {
                return bettor.name + " didn't place bet";
            }
        }

        public int payOut(int winner)
        {
            if (winner == dog)
            {
                return amount;
            }
            else
            {
                return (amount * -1);
            }
        }
    }
}
