# Racing Simulator
.Net application about betting for greyhounds and simulating their race.
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman
## Author
* **Patryk Kupis** - [Patryk Kupis](https://gitlab.com/Kupis)
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details